package geometry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void verifygetAreaTriangle() {
        Triangle triangle = new Triangle(3, 3);

        assertEquals("Equals", 4.5, triangle.getArea(), 0.0001);

    }

    @Test
    public void verifygetHeight() {
        Triangle triangle = new Triangle(0, 0);
        assertEquals("grabbing the height", triangle.getHeight(), 0, 0.0001);
    }

    public void verifygetLength() {
        Triangle triangle = new Triangle(0, 0);
        assertEquals("grabbing the length", triangle.getWidth(), 0, 0.0001);
    }
    @Test
    public void Testing_GetArea_of_Square(){
        
        Square square= new Square(5);
        assertEquals("equal?",25,square.getArea(),0.0001); 
    }
    @Test 
    public void Testing_Square_ToString(){
        Square square = new Square (55);
        assertEquals("Area of Square: "+square.getArea(),square.toString());
    }
    @Test
    public void Testing_Getter_Sides(){
        Square square = new Square(3);
        assertEquals(square.getSide(),square.getSide(),0.0001);
    }
}
