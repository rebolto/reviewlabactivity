package geometry;

public class Triangle {

    private double height;
    private double length;

    public Triangle(double height, double length) {
        this.height = height;
        this.length = length;

    }

    public double getArea() {
        return ((this.height * this.length) / 2);
    }

    public String toString() {
        return "This Triangle has a area of: " + getArea();
    }

    public double getHeight() {

        return this.height;
    }

    public double getWidth() {
        return this.length;
    }
}
