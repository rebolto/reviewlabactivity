package geometry;

public class Square {
    private double sides;
    public Square(double sides){
        this.sides=sides;
    }
    public double getArea(){
        return (this.sides*this.sides);
    }
    public String toString(){
        return "Area of Square: "+getArea();
    }
    public double getSide(){
        return this.sides;
    }
}
